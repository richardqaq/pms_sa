from django.urls import path,re_path
from django.conf.urls import url
from myapp import views

urlpatterns = [
    path('test01/', views.test01),
    # re_path(r'^test02/$',views.test02),
    path('test02/',views.test02),
    path('test03/',views.test03),
    path('start/',views.start),
    # path('end/',views.end),
    path('end/',views.end,name='end'),
    path('findAll/',views.test_find_all),
    path('add/',views.test_add),
    path('update/',views.test_update),
    path('delete/',views.test_delete),
    path('upload/',views.upload_file),

    path('getUnits',views.getUnits),
    path('getSystems',views.getSystems),
    path('getDomains',views.getDomains),
]
