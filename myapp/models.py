# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TUser(models.Model):
    id = models.IntegerField(primary_key=True)
    username = models.TextField()  # This field type is a guess.
    address = models.TextField()  # This field type is a guess.
    birthday = models.TextField()  # This field type is a guess.
    salary = models.TextField()  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't_user'

class SaUnit(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'sa_unit'

class SaSystem(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'sa_system'

class SaDomain(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'sa_domain'
